#ifndef POLYCOMMON_H
#define POLYCOMMON_H 1

#include <elm/util/BitVector.h>
#include <elm/log/Log.h>
#include <otawa/cfg.h>
#include <otawa/cfg/features.h>
#include <otawa/ipet.h>
#include <otawa/otawa.h>
#include <otawa/prog/sem.h>
#include <ppl.hh>

#include "MyHTable.h"


#undef POLY_DEBUG
//#define POLY_DEBUG 1
#ifdef POLY_DEBUG
	#define PDBG(x) { cout << x; }
#else
	#define PDBG(x) { }
#endif

#undef DBG
#define DBG DBGLN

namespace otawa {
namespace poly {
using namespace otawa;
using namespace otawa::util;

class PPLDomain;
extern Identifier<int> NUM_LOC_VARS;
extern Identifier<int> LOC_VAR_SIZE;
extern Identifier<int> MAX_AXIS;
extern Identifier<bool> SUMMARIZE;
extern Identifier<PPLDomain*> SUMMARY;
extern Identifier<MyHTable<int, PPLDomain>* > MAX_LINEAR;
extern p::feature POLY_ANALYSIS_FEATURE;
} // namespace poly
} // namespace otawa
#endif
