/*
 * $Id$
 * Copyright (c) 2005 IRIT-UPS
 *
 * include/util/PostDominance.h -- PostDominance class interface.
 */
#ifndef OTAWA_UTIL_POSTDOMINANCE_H
#define OTAWA_UTIL_POSTDOMINANCE_H

#include <otawa/cfg/PostDominance.h>
#warning "<otawa/util/PostDominance.h> deprecated: use <otawa/cfg/PostDominance.h> instead."

#endif // OTAWA_UTIL_POSTDOMINANCE_H
